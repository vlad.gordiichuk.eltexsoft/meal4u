//
//  AuthView.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

struct AuthView: View {

    // MARK: - Views

    var body: some View {

//        NavigationView {

            AuthBaseView {

                VStack(spacing: 35) {

                    // swiftlint:disable line_length
                    Text("We are happy to see you got this far. However, we need to know you a little bit better, before you can proceed to the payment. Please log in with your mobile number, social media profiles, or create a new user below.\n\n- It only takes a few minutes.")
                        .multilineTextAlignment(.center)
                        .font(FontFamily.Roboto.regular.swiftUIFont(size: 16))
                        .foregroundColor(Asset.darkSilver.swiftUIColor)
                        .fixedSize(
                            horizontal: false,
                            vertical: true
                        )

                    VStack(spacing: 20) {

                        VStack(spacing: 10) {

                            Button { } label: {

                                ZStack {

                                    Asset.mustard.swiftUIColor
                                        .frame(height: 55)
                                        .clipShape(Capsule())

                                    HStack(spacing: 5) {

                                        SystemImage.iphone.swiftUIImage
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 20, height: 20)

                                        Text("Sign Up with mobile number")
                                            .font(FontFamily.Roboto.medium.swiftUIFont(size: 14))
                                            .multilineTextAlignment(.center)
                                            .lineLimit(1)
                                            .frame(maxWidth: .infinity, alignment: .center)
                                            .padding(.trailing, 25)
                                    }
                                    .padding(.horizontal, 20)
                                }
                                .foregroundColor(Asset.white.swiftUIColor)
                            }

                            Button { } label: {

                                ZStack {

                                    Asset.cyanBlueAzure.swiftUIColor
                                        .frame(height: 55)
                                        .clipShape(Capsule())

                                    HStack(spacing: 5) {

                                        Asset.iconFacebook.swiftUIImage
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 20, height: 20)

                                        Text("Sign In with Facebook")
                                            .font(FontFamily.Roboto.medium.swiftUIFont(size: 14))
                                            .multilineTextAlignment(.center)
                                            .lineLimit(1)
                                            .frame(maxWidth: .infinity, alignment: .center)
                                            .padding(.trailing, 25)
                                    }
                                    .padding(.horizontal, 20)
                                }
                                .foregroundColor(Asset.white.swiftUIColor)
                            }

                            Button { } label: {

                                ZStack {

                                    Asset.cinnabar.swiftUIColor
                                        .frame(height: 55)
                                        .clipShape(Capsule())

                                    HStack(spacing: 5) {

                                        Asset.iconGoogle.swiftUIImage
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 20, height: 20)

                                        Text("Sign In with Google")
                                            .font(FontFamily.Roboto.medium.swiftUIFont(size: 14))
                                            .multilineTextAlignment(.center)
                                            .lineLimit(1)
                                            .frame(maxWidth: .infinity, alignment: .center)
                                            .padding(.trailing, 25)
                                    }
                                    .padding(.horizontal, 20)
                                }
                                .foregroundColor(Asset.white.swiftUIColor)
                            }

                            Button { } label: {

                                ZStack {

                                    Asset.quartz.swiftUIColor
                                        .frame(height: 55)
                                        .clipShape(Capsule())

                                    HStack(spacing: 5) {

                                        SystemImage.appleLogo.swiftUIImage
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 20, height: 20)

                                        Text("Sign In with Apple")
                                            .font(FontFamily.Roboto.medium.swiftUIFont(size: 14))
                                            .multilineTextAlignment(.center)
                                            .lineLimit(1)
                                            .frame(maxWidth: .infinity, alignment: .center)
                                            .padding(.trailing, 25)
                                    }
                                    .padding(.horizontal, 20)
                                }
                                .foregroundColor(Asset.white.swiftUIColor)
                            }
                        }

                        NavigationLink(destination: {

                            AuthBaseView {

                                Text("Login")
                            }
                        }, label: {

                            Text("Already Created?")
                                .font(FontFamily.Roboto.regular.swiftUIFont(size: 16))
                            +
                            Text(" ")
                                .font(FontFamily.Roboto.regular.swiftUIFont(size: 16))
                            +
                            Text("Login here")
                                .font(FontFamily.Roboto.bold.swiftUIFont(size: 16))
                                .underline()
                        })
                        .foregroundColor(Asset.philippineGray.swiftUIColor)
                        .fixedSize(
                            horizontal: false,
                            vertical: true
                        )
                    }
                }
                .padding(.horizontal, 45)
                .padding(.bottom, 10)
            }
//            .edgesIgnoringSafeArea(.top)
            .navigationBarTitle("Main")
//        }
//        .applyAuthNavigationViewStyle()
    }
}

struct AuthView_Previews: PreviewProvider {
    static var previews: some View {

        AuthView()
            .environment(\.locale, Locale(identifier: "en-US"))

        AuthView()
            .environment(\.locale, Locale(identifier: "da-DK"))

        AuthView()
            .preferredColorScheme(.dark)
    }
}

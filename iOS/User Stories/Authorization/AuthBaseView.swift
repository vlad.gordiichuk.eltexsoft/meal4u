//
//  AuthBaseView.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/5/22.
//

import SwiftUI

struct AuthBaseView<Content: View>: View {

    // MARK: - View Variables

    let content: Content

    @State private var logoHeight: CGFloat = 0

    private var logoAndContentSeparatorHeight: CGFloat = 35

    // MARK: - Init

    init(@ViewBuilder content: () -> Content) {

        self.content = content()
    }

    // MARK: - Views

    var body: some View {

        GeometryReader { outerGeometryProxy in

            ScrollView {

                VStack(spacing: 0) {

                    Asset.backgroundTop.swiftUIImage
                        .resizable()
                        .scaledToFill()
                        .frame(
                            height: logoHeight,
                            alignment: .bottom
                        )

                    Spacer()
                        .frame(height: logoAndContentSeparatorHeight)

                    VStack {

//                        Color.red//.frame(height: 300)
                        content
//                            .frame(maxHeight: .infinity)
                    }
                    .overlay(

                        GeometryReader { proxy in

                            Color.clear.preference(
                                key: HeightPreferenceKey.self,
                                value: proxy.size.height
                            )
                        }
                    )
                }
                .frame(
                    minHeight: outerGeometryProxy.size.height// + outerGeometryProxy.safeAreaInsets.top
                )

            }
            .onPreferenceChange(HeightPreferenceKey.self) { contentHeight in

                print(contentHeight)
                let minImageHeight: CGFloat = 150 + outerGeometryProxy.safeAreaInsets.top

                let imageSize = Asset.backgroundTop.image.size
                let imageAspectRatio = imageSize.height / imageSize.width
                let maxImageHeight = (imageAspectRatio * outerGeometryProxy.size.width)

                let preferredHeight = outerGeometryProxy.size.height +
                    outerGeometryProxy.safeAreaInsets.top -
                    contentHeight -
                    logoAndContentSeparatorHeight

                logoHeight = max(
                    minImageHeight,
                    min(
                        maxImageHeight,
                        preferredHeight
                    )
                )
            }
            .introspectScrollView { scrollView in

                scrollView.alwaysBounceVertical = false
            }
            .edgesIgnoringSafeArea(.top)
        }
    }
}

struct AuthBaseView_Previews: PreviewProvider {
    static var previews: some View {

        AuthBaseView {
            Asset.cinnabar.swiftUIColor
        }
        .environment(\.locale, Locale(identifier: "en-US"))

        AuthBaseView {
            Asset.cinnabar.swiftUIColor
        }
        .environment(\.locale, Locale(identifier: "da-DK"))

        AuthBaseView {
            Asset.cinnabar.swiftUIColor
        }
        .preferredColorScheme(.dark)
    }
}

struct HeightPreferenceKey: PreferenceKey {

    typealias Value = CGFloat

    static var defaultValue: CGFloat = 0

    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {

        value = nextValue()
    }
}

//
//  ProfileViewItemView.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

struct ProfileViewSectionView: View {

    // MARK: - Variables

    var section: ProfileViewSection

    // MARK: - Views

    var body: some View {

        Text(section.getTitle())
            .font(FontFamily.Roboto.regular.swiftUIFont(size: 14))
            .textCase(.uppercase)
            .padding(.vertical, 10)
            .padding(.horizontal, 45)
            .frame(maxWidth: .infinity, alignment: .leading)
            .background(Asset.mustard.swiftUIColor.opacity(0.1))
    }
}

struct ProfileViewSectionView_Previews: PreviewProvider {
    static var previews: some View {

        Group {

            ProfileViewSectionView(
                section: .profile
            )
            .environment(\.locale, Locale(identifier: "en"))

            ProfileViewSectionView(
                section: .profile
            )
            .environment(\.locale, Locale(identifier: "da"))

            ProfileViewSectionView(
                section: .profile
            )
            .preferredColorScheme(.dark)
        }
        .previewLayout(.sizeThatFits)
        .padding()
    }
}

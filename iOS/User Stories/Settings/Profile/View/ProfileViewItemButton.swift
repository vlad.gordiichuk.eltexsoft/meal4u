//
//  ProfileViewItemButton.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

struct ProfileViewItemButton: View {

    // MARK: - Variables

    var item: ProfileViewItem

    // MARK: - Views

    var body: some View {

        HStack(spacing: 20) {

            item.getImage()
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 32, height: 32, alignment: .center)
                .foregroundColor(Asset.mustard.swiftUIColor)

            HStack(spacing: 0) {

                Text(item.getTitle())
                    .font(FontFamily.Roboto.regular.swiftUIFont(size: 18))
                    .foregroundColor(Asset.darkSilver.swiftUIColor)
                    .lineLimit(1)

                Spacer(minLength: 20)

                if let valueText = item.getValue() {

                    Text(valueText)
                        .font(FontFamily.Roboto.bold.swiftUIFont(size: 18))
                        .foregroundColor(Asset.silverSand.swiftUIColor)
                        .lineLimit(1)
                        .layoutPriority(-0.1)
                }
            }

            SystemImage.chevronForward.swiftUIImage
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 17)
                .foregroundColor(Asset.silverSand.swiftUIColor)
        }
        .padding(20)
        .background(Asset.white.swiftUIColor)
        .cornerRadius(10)
        .shadow(color: Asset.black.swiftUIColor.opacity(0.15), radius: 2, y: 1)
    }
}

struct ProfileViewItemButton_Previews: PreviewProvider {
    static var previews: some View {

        Group {

            ProfileViewItemButton(
                item: .name
            )
            .environment(\.locale, Locale(identifier: "en-US"))

            ProfileViewItemButton(
                item: .name
            )
            .environment(\.locale, Locale(identifier: "da-DK"))

            ProfileViewItemButton(
                item: .name
            )
            .preferredColorScheme(.dark)
        }
        .previewLayout(.sizeThatFits)
        .padding()
    }
}

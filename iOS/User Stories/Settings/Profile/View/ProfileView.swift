//
//  ProfileView.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

struct ProfileView: View {

    // MARK: - Views

    var body: some View {

        ScrollView {

            VStack(spacing: 15) {

                ForEach(ProfileViewSection.allCases, id: \.self) { section in

                    ProfileViewSectionView(section: section)

                    Group {

                        ForEach(section.getItems(), id: \.self) { item in

                            Button { } label: {

                                ProfileViewItemButton(item: item)
                            }
                            .buttonStyleOpacityChangeOnTap()
                        }
                    }
                    .padding(.horizontal, 25)
                }

                Text(L10n.ProfileView.versionLabel(UIApplication.appVersion))
                .font(FontFamily.Roboto.regular.swiftUIFont(size: 18))
                .foregroundColor(Asset.silverSand.swiftUIColor)
            }
            .padding(.top, 20)
            .padding(.bottom, 40)
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {

        ProfileView()
            .environment(\.locale, Locale(identifier: "en-US"))

        ProfileView()
            .environment(\.locale, Locale(identifier: "da-DK"))

        ProfileView()
            .preferredColorScheme(.dark)
    }
}

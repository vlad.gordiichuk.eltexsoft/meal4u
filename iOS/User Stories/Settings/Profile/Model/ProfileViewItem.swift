//
//  ProfileViewItem.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

// swiftlint:disable cyclomatic_complexity

import SwiftUI

enum ProfileViewItem {

    // MARK: - Cases

    case name,
         phone,
         email,
         creditCards,
         adresses,
         orderHistory,
         changePassword,
         deleteAccount,
         aboutUs,
         termsAndConditions,
         privacyPolicy,
         signOut

    // MARK: - Interaction Methods

    public func getTitle() -> LocalizedStringKey {

        let sectionItemsLocaleKeys = L10n.ProfileView.SectionItems.self

        switch self {
        case .name: return sectionItemsLocaleKeys.nameLabel.key
        case .phone: return sectionItemsLocaleKeys.phoneLabel.key
        case .email: return sectionItemsLocaleKeys.emailLabel.key
        case .creditCards: return sectionItemsLocaleKeys.creditCardsLabel.key
        case .adresses: return sectionItemsLocaleKeys.addressesLabel.key
        case .orderHistory: return sectionItemsLocaleKeys.ordersLabel.key
        case .changePassword: return sectionItemsLocaleKeys.changePassswordLabel.key
        case .deleteAccount: return sectionItemsLocaleKeys.deleteAccountLabel.key
        case .aboutUs: return sectionItemsLocaleKeys.aboutUsLabel.key
        case .termsAndConditions: return sectionItemsLocaleKeys.termsAndConditionsLabel.key
        case .privacyPolicy: return sectionItemsLocaleKeys.privacyPolicyLabel.key
        case .signOut: return sectionItemsLocaleKeys.signOutLabel.key
        }
    }

    public func getImage() -> Image {

        switch self {
        case .name: return SystemImage.personCropCircle.swiftUIImage
        case .phone: return SystemImage.iphoneRadiowavesLeftAndRight.swiftUIImage
        case .email: return SystemImage.envelopeCircle.swiftUIImage
        case .creditCards: return SystemImage.creditCardCircle.swiftUIImage
        case .adresses: return SystemImage.houseCircle.swiftUIImage
        case .orderHistory: return SystemImage.cartCircle.swiftUIImage
        case .changePassword: return SystemImage.lockCircle.swiftUIImage
        case .deleteAccount: return SystemImage.personCropCircleBadgeMinus.swiftUIImage
        case .aboutUs: return SystemImage.bookClosedCircle.swiftUIImage
        case .termsAndConditions: return SystemImage.bookClosedCircle.swiftUIImage
        case .privacyPolicy: return SystemImage.bookClosedCircle.swiftUIImage
        case .signOut: return SystemImage.rectanglePortraitAndArrowRight.swiftUIImage
        }
    }

    public func getValue() -> String? {

        switch self {
        case .name: return "John Appleseed"
        case .phone: return "12 34 56 78"
        case .email: return "john.appleseed@apple.com"
        default: return nil
        }
    }
}

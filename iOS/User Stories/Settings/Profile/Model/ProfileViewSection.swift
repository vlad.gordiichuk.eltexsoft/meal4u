//
//  ProfileViewSection.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

enum ProfileViewSection: CaseIterable {

    // MARK: - Cases

    case profile,
         account,
         information

    // MARK: - Interaction Methods

    public func getTitle() -> LocalizedStringKey {

        let sectionLocaleKeys = L10n.ProfileView.Sections.self

        switch self {
        case .profile: return sectionLocaleKeys.profileLabel.key
        case .account: return sectionLocaleKeys.accountLabel.key
        case .information: return sectionLocaleKeys.informationLabel.key
        }
    }

    public func getItems() -> [ProfileViewItem] {

        switch self {
        case .profile:

            return [
                .name,
                .phone,
                .email,
                .creditCards,
                .adresses,
                .orderHistory
            ]
        case .account:

            return [
                .changePassword,
                .deleteAccount
            ]
        case .information:

            return [
                .aboutUs,
                .termsAndConditions,
                .privacyPolicy,
                .signOut
            ]
        }
    }
}

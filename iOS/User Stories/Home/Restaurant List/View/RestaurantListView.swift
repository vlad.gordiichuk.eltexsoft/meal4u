//
//  RestaurantListView.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

struct RestaurantListView: View {

    // MARK: - Views

    var body: some View {

        Text("Restaurant List View")
    }
}

struct RestaurantListView_Previews: PreviewProvider {
    static var previews: some View {

        RestaurantListView()
            .environment(\.locale, Locale(identifier: "en-US"))

        RestaurantListView()
            .environment(\.locale, Locale(identifier: "da-DK"))

        RestaurantListView()
            .preferredColorScheme(.dark)
    }
}

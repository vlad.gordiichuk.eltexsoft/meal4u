//
//  GeneralTabView.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/1/22.
//

import SwiftUI

struct GeneralTabView: View {

    // MARK: - Variables

    @State private var selectedTabBarItem = TabBarItem.home

    // MARK: - Views

    var body: some View {

        VStack(spacing: 0) {

            TabView(selection: $selectedTabBarItem) {

                NavigationView {

                    RestaurantListView()
                        .navigationBarHidden(true)
                        .navigationTitle(L10n.NavigationBar.homeLabel.key)
                }
                .applyGeneralNavigationViewStyle()
                .navigationViewStyle(.stack)
                .tag(TabBarItem.home)

                NavigationView {

                    ProfileView()
                        .navigationBarHidden(false)
                        .navigationTitle(L10n.NavigationBar.profileAndSettingsLabel.key)
                }
                .applyGeneralNavigationViewStyle()
                .navigationViewStyle(.stack)
                .tag(TabBarItem.profileAndSettings)
            }
            .introspectTabBarController { tabBarController in
                tabBarController.tabBar.isHidden = true
            }

            GeneralTabBarView(selectedTabBarItem: $selectedTabBarItem)
        }
    }
}

struct GeneralTabView_Previews: PreviewProvider {
    static var previews: some View {

        GeneralTabView()
            .environment(\.locale, Locale(identifier: "en-US"))

        GeneralTabView()
            .environment(\.locale, Locale(identifier: "da-DK"))

        GeneralTabView()
            .preferredColorScheme(.dark)
    }
}

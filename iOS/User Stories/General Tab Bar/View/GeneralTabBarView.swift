//
//  GeneralTabBarView.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 8/30/22.
//

import SwiftUI

struct GeneralTabBarView: View {

    // MARK: - Variables

    @Binding var selectedTabBarItem: TabBarItem

    @State private var isShowingAuthView = false

    // MARK: - Views

    var firstLayer: some View {
        GeometryReader { proxy in

            let width = proxy.size.width
            let height = proxy.size.height

            let midX = proxy.size.width / 2

            let ovalRadius: CGFloat = 40

            Path { path in
                path.move(to: .zero)
                path.addArc(
                    center: CGPoint(x: midX, y: 10),
                    radius: ovalRadius,
                    startAngle: .degrees(194),
                    endAngle: .degrees(-14),
                    clockwise: false
                )
                path.addLine(to: CGPoint(x: width, y: 0))
                path.addLine(to: CGPoint(x: width, y: height))
                path.addLine(to: CGPoint(x: 0, y: height))
                path.closeSubpath()
            }
            .fill(Asset.white.swiftUIColor)
            .shadow(color: Asset.black.swiftUIColor.opacity(0.1), radius: 5, y: -1)
            .mask { Asset.white.swiftUIColor.padding(.top, -40) }
        }
    }

    var secondLayer: some View {
        GeometryReader { proxy in

            let width = proxy.size.width
            let height = proxy.size.height

            let midX = proxy.size.width / 2

            let ovalRadius: CGFloat = 40

            Path { path in
                path.move(to: CGPoint(x: 0, y: 1))
                path.addLine(to: CGPoint(x: midX - ovalRadius - 20, y: 1))
                path.addQuadCurve(
                    to: CGPoint(x: midX - ovalRadius + 1, y: 20),
                    control: CGPoint(x: midX - ovalRadius, y: 1)
                )
                path.addArc(
                    center: CGPoint(x: midX, y: 10),
                    radius: ovalRadius,
                    startAngle: .degrees(166),
                    endAngle: .degrees(14),
                    clockwise: true
                )
                path.addQuadCurve(
                    to: CGPoint(x: midX + ovalRadius + 20, y: 1),
                    control: CGPoint(x: midX + ovalRadius, y: 1)
                )
                path.addLine(to: CGPoint(x: width, y: 1))
                path.addLine(to: CGPoint(x: width, y: height))
                path.addLine(to: CGPoint(x: 0, y: height))
                path.closeSubpath()
            }
            .fill(
                .linearGradient(
                    colors: [
                        Asset.platinum.swiftUIColor,
                        Asset.white.swiftUIColor
                    ],
                    startPoint: .top,
                    endPoint: .bottom
                )
            )
        }
    }

    var homeButton: some View {

        GeometryReader { proxy in

            Button {

                selectedTabBarItem = .home
            } label: {

                Asset.logoSmall.swiftUIImage
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .padding(.vertical, 8)
                    .padding(.horizontal, 16)
                    .frame(width: 70, height: 70)
                    .background(Asset.mustard.swiftUIColor)
                    .clipShape(Circle())
            }
            .position(
                x: proxy.size.width / 2,
                y: 10
            )
        }
    }

    var loginButton: some View {

        Button {

            isShowingAuthView = true
        } label: {
            Label(
                L10n.TabBar.loginButton.key,
                systemImage: SystemImage.personCropCircle.rawValue
            )
            .applyTabBarLabelStyle()
        }
        .sheet(isPresented: $isShowingAuthView) {

            NavigationView {

                AuthView()
            }
            .applyAuthNavigationViewStyle()
        }
    }

    var profileButton: some View {

        Button {

            selectedTabBarItem = .profileAndSettings
        } label: {
            Label(
                L10n.TabBar.profileButton.key,
                systemImage: SystemImage.ellipsisCircle.rawValue
            )
            .applyTabBarLabelStyle()
        }
    }

    var body: some View {
        ZStack(alignment: .center) {

            firstLayer

            secondLayer

            HStack(alignment: .center, spacing: 5) {
                Group {
                    Spacer()

                    Spacer()

                    homeButton

                    Spacer()

                    if true {

                        loginButton
                    } else {

                        profileButton
                    }
                }
                .frame(maxWidth: .infinity)
            }
            .padding(.horizontal, 5)
        }
        .frame(height: 60)
    }
}

struct GeneralTabBarView_Previews: PreviewProvider {
    static var previews: some View {

        Group {

            GeneralTabBarView(
                selectedTabBarItem: .constant(.home)
            )
            .environment(\.locale, Locale(identifier: "en-US"))

            GeneralTabBarView(
                selectedTabBarItem: .constant(.home)
            )
            .environment(\.locale, Locale(identifier: "da-DK"))

            GeneralTabBarView(
                selectedTabBarItem: .constant(.home)
            )
            .preferredColorScheme(.dark)
        }
        .previewLayout(.sizeThatFits)
        .padding(.top, 70)
    }
}

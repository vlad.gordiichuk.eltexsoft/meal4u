//
//  TabBarItem.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import Foundation

enum TabBarItem {
    case home, profileAndSettings
}

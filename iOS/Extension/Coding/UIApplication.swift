//
//  UIApplication.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

extension UIApplication {

    // MARK: - App Version

    static var appVersion: String {

        let bundle = Bundle.main
        let keyName = "CFBundleShortVersionString"
        let value = bundle.object(forInfoDictionaryKey: keyName) as? String

        return value ?? "- - -"
    }
}

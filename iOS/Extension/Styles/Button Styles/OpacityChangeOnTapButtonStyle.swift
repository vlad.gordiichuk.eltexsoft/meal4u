//
//  OpacityChangeOnTapButtonStyle.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

extension View {

    public func buttonStyleOpacityChangeOnTap() -> some View {

        buttonStyle(OpacityChangeOnTapButtonStyle())
    }
}

struct OpacityChangeOnTapButtonStyle: ButtonStyle {

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .opacity(configuration.isPressed ? 0.8 : 1)
    }
}

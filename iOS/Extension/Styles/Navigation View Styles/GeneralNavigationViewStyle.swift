//
//  GeneralNavigationViewStyle.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

extension NavigationView {

    public func applyGeneralNavigationViewStyle() -> some View {

        modifier(GeneralNavigationViewStyle())
    }
}

struct GeneralNavigationViewStyle: ViewModifier {

    func body(content: Content) -> some View {

        content
            .introspectNavigationController { navigationController in

                let targetColor = UIColor(asset: Asset.black)

                let appearance = UINavigationBarAppearance()

                appearance.configureWithOpaqueBackground()

                appearance.titleTextAttributes = [
                    .font: FontFamily.Roboto.medium.font(size: 18),
                    .foregroundColor: targetColor as Any
                ]

                let button = UIBarButtonItemAppearance(style: .plain)
                button.normal.titleTextAttributes = [
                    .font: FontFamily.Roboto.regular.font(size: 18),
                    .foregroundColor: targetColor as Any
                ]
                appearance.buttonAppearance = button

                let done = UIBarButtonItemAppearance(style: .done)
                done.normal.titleTextAttributes = [
                    .font: FontFamily.Roboto.medium.font(size: 18),
                    .foregroundColor: targetColor as Any
                ]
                appearance.doneButtonAppearance = done

                navigationController.navigationBar.compactAppearance = appearance
                navigationController.navigationBar.scrollEdgeAppearance = appearance
                navigationController.navigationBar.standardAppearance = appearance

                navigationController.navigationBar.tintColor = targetColor

                navigationController.navigationBar.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
                navigationController.navigationBar.layer.shadowOpacity = 10
                navigationController.navigationBar.layer.shadowOffset.height = 4
                navigationController.navigationBar.layer.shadowRadius = 2
            }
    }
}

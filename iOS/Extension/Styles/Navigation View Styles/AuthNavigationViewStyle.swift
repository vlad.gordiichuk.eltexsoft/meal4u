//
//  AuthNavigationViewStyle.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/5/22.
//

import SwiftUI

extension NavigationView {

    public func applyAuthNavigationViewStyle() -> some View {

        modifier(AuthNavigationViewStyle())
    }
}

struct AuthNavigationViewStyle: ViewModifier {

    func body(content: Content) -> some View {

        content
            .introspectNavigationController { navigationController in

                let targetColor = UIColor(asset: Asset.white)

                let appearance = UINavigationBarAppearance()

                appearance.configureWithTransparentBackground()

                appearance.titleTextAttributes = [
                    .font: FontFamily.Roboto.medium.font(size: 18),
                    .foregroundColor: UIColor.clear
                ]

                let button = UIBarButtonItemAppearance(style: .plain)
                button.normal.titleTextAttributes = [
                    .font: FontFamily.Roboto.regular.font(size: 18),
                    .foregroundColor: targetColor as Any
                ]
                appearance.buttonAppearance = button

                let done = UIBarButtonItemAppearance(style: .done)
                done.normal.titleTextAttributes = [
                    .font: FontFamily.Roboto.medium.font(size: 18),
                    .foregroundColor: targetColor as Any
                ]
                appearance.doneButtonAppearance = done

                navigationController.navigationBar.compactAppearance = appearance
                navigationController.navigationBar.scrollEdgeAppearance = appearance
                navigationController.navigationBar.standardAppearance = appearance

                navigationController.navigationBar.tintColor = targetColor
                navigationController.navigationBar.prefersLargeTitles = false
            }
    }
}

//
//  TabBarLabelStyle.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/2/22.
//

import SwiftUI

extension Label {

    public func applyTabBarLabelStyle() -> some View {

        labelStyle(TabBarLabelStyle())
    }
}

struct TabBarLabelStyle: LabelStyle {

    func makeBody(configuration: Configuration) -> some View {

        VStack(alignment: .center, spacing: 2) {

            configuration.icon
                .font(.system(size: 20))
                .aspectRatio(contentMode: .fit)
                .frame(width: 25, height: 25)

            configuration.title
                .font(FontFamily.Roboto.regular.swiftUIFont(fixedSize: 13))
                .lineLimit(1)
        }
        .foregroundColor(Asset.darkSilver.swiftUIColor)
    }
}

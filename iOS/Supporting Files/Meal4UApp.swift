//
//  Meal4UApp.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 8/15/22.
//

import SwiftUI
import Introspect

@main
struct Meal4UApp: App {
    var body: some Scene {
        WindowGroup {
            GeneralTabView()
        }
    }
}

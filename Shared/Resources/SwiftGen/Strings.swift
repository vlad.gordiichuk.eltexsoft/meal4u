// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable all

import SwiftUI

// MARK: - Strings

public enum L10n {
  public enum NavigationBar {
    /// Home
    public static let homeLabel = LocalizedString(lookupKey: "navigationBar.homeLabel")
    /// Profile & Settings
    public static let profileAndSettingsLabel = LocalizedString(lookupKey: "navigationBar.profileAndSettingsLabel")
  }
  public enum ProfileView {
    /// Version %@
    public static func versionLabel(_ p1: Any) -> String {
      return L10n.tr("Localizable", "profileView.versionLabel %@", String(describing: p1))
    }
    public enum SectionItems {
      /// About Us
      public static let aboutUsLabel = LocalizedString(lookupKey: "profileView.sectionItems.aboutUsLabel")
      /// My Addresses
      public static let addressesLabel = LocalizedString(lookupKey: "profileView.sectionItems.addressesLabel")
      /// Change Password
      public static let changePassswordLabel = LocalizedString(lookupKey: "profileView.sectionItems.changePassswordLabel")
      /// My Credit Cards
      public static let creditCardsLabel = LocalizedString(lookupKey: "profileView.sectionItems.creditCardsLabel")
      /// Delete Account
      public static let deleteAccountLabel = LocalizedString(lookupKey: "profileView.sectionItems.deleteAccountLabel")
      /// Email
      public static let emailLabel = LocalizedString(lookupKey: "profileView.sectionItems.emailLabel")
      /// Name
      public static let nameLabel = LocalizedString(lookupKey: "profileView.sectionItems.nameLabel")
      /// My Orders
      public static let ordersLabel = LocalizedString(lookupKey: "profileView.sectionItems.ordersLabel")
      /// Phone
      public static let phoneLabel = LocalizedString(lookupKey: "profileView.sectionItems.phoneLabel")
      /// Privacy Policy
      public static let privacyPolicyLabel = LocalizedString(lookupKey: "profileView.sectionItems.privacyPolicyLabel")
      /// Log Out
      public static let signOutLabel = LocalizedString(lookupKey: "profileView.sectionItems.signOutLabel")
      /// Terms & Conditions
      public static let termsAndConditionsLabel = LocalizedString(lookupKey: "profileView.sectionItems.termsAndConditionsLabel")
    }
    public enum Sections {
      /// Account
      public static let accountLabel = LocalizedString(lookupKey: "profileView.sections.accountLabel")
      /// Information
      public static let informationLabel = LocalizedString(lookupKey: "profileView.sections.informationLabel")
      /// Profile
      public static let profileLabel = LocalizedString(lookupKey: "profileView.sections.profileLabel")
    }
  }
  public enum TabBar {
    /// Login
    public static let loginButton = LocalizedString(lookupKey: "tabBar.loginButton")
    /// Profile
    public static let profileButton = LocalizedString(lookupKey: "tabBar.profileButton")
  }
}

// MARK: - Implementation Details

extension L10n {
  fileprivate static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

public struct LocalizedString {
  internal let lookupKey: String

  var key: LocalizedStringKey {
    LocalizedStringKey(lookupKey)
  }

  var text: String {
    L10n.tr("Localizable", lookupKey)
  }
}

private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
      return Bundle.module
    #else
      return Bundle(for: BundleToken.self)
    #endif
  }()
}

//
//  SystemImages.swift
//  Meal4U
//
//  Created by Vlad Gordiichuk on 9/1/22.
//

import SwiftUI

enum SystemImage: String {

    case appleLogo = "applelogo"

    case personCropCircle = "person.crop.circle"

    case personCropCircleBadgeMinus = "person.crop.circle.badge.minus"

    case ellipsisCircle = "ellipsis.circle"

    case envelopeCircle = "envelope.circle"

    case iphone = "iphone"

    case iphoneRadiowavesLeftAndRight = "iphone.radiowaves.left.and.right"

    case creditCardCircle = "creditcard.circle"

    case cartCircle = "cart.circle"

    case chevronForward = "chevron.forward"

    case houseCircle = "house.circle"

    case lockCircle = "lock.circle"

    case bookClosedCircle = "book.closed.circle"

    case rectanglePortraitAndArrowRight = "rectangle.portrait.and.arrow.right"

    // MARK: - Interaction Methods

    var swiftUIImage: SwiftUI.Image {

        SwiftUI.Image(systemName: rawValue)
    }
}
